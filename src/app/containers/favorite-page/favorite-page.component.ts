import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Book} from '../../models/book';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../reducers';

@Component({
  selector: 'bc-favorite-page',
  templateUrl: './favorite-page.component.html',
  styleUrls: ['./favorite-page.component.css']
})
export class FavoritePageComponent implements OnInit {

  books$: Observable<Book[]>;

  constructor(store: Store<fromRoot.State>) {
    this.books$ = store.select(fromRoot.getBookCollection);
  }
  ngOnInit(): void {
  }

}
